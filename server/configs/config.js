module.exports = {
	dbProd: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/investor-challenge',
	dbTest: process.env.MONGOLAB_URI || 'mongodb://localhost:27017/investor-challenge-test',
	mongooseCfg: { useNewUrlParser: true, useCreateIndex: true },
	nameMinLength: 3,
	minAge: 18,
	maxAge: 100,
	minMonthlyIncome: 3000,
	maxMonthlyIncome: 20000,
	minLatitude: -90,
	maxLatitude: 90,
	rangeLatitude: 180, // 90 - (-90)
	minLongitude: -180,
	maxLongitude: 180,
	rangeLongitude: 360, // 180 - (-180)
	searchRadius: 10, 
	earthRadiusMiles: 3963.2,
	earthRadiusKms: 6378.1,
	resultLimit: 10,
	decimalDigits: 1
};

// mongodb://root:3R6XedL4Jpkbo4WT@mongodbsg-shard-00-00-2sebm.mongodb.net:27017,mongodbsg-shard-00-01-2sebm.mongodb.net:27017,mongodbsg-shard-00-02-2sebm.mongodb.net:27017/investor-challenge?ssl=true&replicaSet=MongoDBSG-shard-0&authSource=admin&retryWrites=true

//Sr^gHt0B40t$HpIf