// load library
var csv = require('fast-csv');
var { ObjectId } = require('mongoose');
const config = require('../configs/config');
// var similarity = require( 'compute-cosine-similarity' );
// var cosSimilarity = require('cos-similarity') ;

// load model
var { Investor } = require('../models/investor');

// save context to self
var investorController = {
	saveInvestors (investors, res) {
		// save investors data to mongodb
	  Investor.insertMany(investors, (err, docs) => {
	    if (err) {
	      return res.status(400).send('Upload file failed');
	    }
	    res.send('Upload investor data success');
	  });
	},
	import (req, res) {
	  if (!req.files)
	    return res.status(400).send('No file were uploaded.');

	  // file data and array for save to db
	  var investorData = req.files.file;
	  var investors = [];

	  // remove all investors
	  Investor.deleteMany({}).then(() => {
	  	// read csv file
	    csv
	      .fromString(investorData.data.toString(), {
	        headers: true, // exclude header (first row)
	        ignoreEmpty: true // ignore empty row
	      })
	      .on('data', (data) => {
	        // push to investors array (formatting)
	        investors.push(new Investor({
	          name: data['name'],
	          age: data['age'],
	          location: { type: 'Point', coordinates: [data['longitude'], data['latitude']] },
	          monthlyIncome: data['monthly income'],
	          experienced: data['experienced']        
	        }));
	      })
	      .on('end', () => {
	        // save bulk investor data to mongodb
	        investorController.saveInvestors(investors, res);
	      })
	  });
	},
	fieldMinMax (req, res) {
		Investor.aggregate([ 
		    { "$group": { 
		        "_id": null,
		        "minAge": { "$min": "$age" },
		        "maxAge": { "$max": "$age" },
		        "minMonthlyIncome": { "$min": "$monthlyIncome" },
		        "maxMonthlyIncome": { "$max": "$monthlyIncome" }
		    }}
		]).then((result) => {
			res.send(result);
		});
	},
	generateVector (age, monthlyIncome, experienced, reqAge, reqMonthlyIncome, reqExperienced) {
		let result = [];

		// add only to vector when profile sent by user
		
		if (reqAge !== undefined) {
			result.push((parseFloat(age) - config.minAge) / (config.maxAge - config.minAge)); // ageProfile
		}

		if (reqMonthlyIncome !== undefined) {
			result.push((parseFloat(monthlyIncome) - config.minMonthlyIncome) / (config.maxMonthlyIncome - config.minMonthlyIncome)); // monthlyIncomeProfile
		}

		if (reqExperienced !== undefined) {
			result.push(experienced == 'true' || experienced === true ? 1 : 0); // experiencedProfile
		}

		// (parseFloat(latitude) - config.minLatitude) / config.rangeLatitude, // latitudeProfile (excluded after score test)
		// (parseFloat(longitude) - config.minLongitude) / config.rangeLongitude // longitudeProfile (excluded after score test)

		return result;
	},
	computeSimilarity (vectorA, vectorB) {
		let accumulate = 0;
		for (var idx in vectorA) {
			// calculate difference (range 0 - 1)
			if (vectorA[idx] > vectorB[idx]) {
				accumulate = accumulate + (vectorA[idx] - vectorB[idx]);
			} else {
				accumulate = accumulate + (vectorB[idx] - vectorA[idx]);
			}
		}
		// return 1 - average difference
		// result range 0 - 1
		return 1 - (accumulate / vectorA.length);
	},
	peopleLikeYou (req, res) {
		if (Object.keys(req.query).length == 0) {
			// minimal 1 get parameter
			return res.send({peopleLikeYou: []});
		} else if (typeof req.query.latitude != typeof req.query.longitude) {
			// validate latitude longitude must be pass together / not pass one of them
			return res.send({peopleLikeYou: []});
		} else if (typeof req.query.age !== 'undefined' && (req.query.age < config.minAge || req.query.age > config.maxAge)) {
			// validate age is in range
			return res.send({peopleLikeYou: []});
		} else if (typeof req.query.monthlyIncome !== 'undefined' && (req.query.monthlyIncome < config.minMonthlyIncome || req.query.monthlyIncome > config.maxMonthlyIncome)) {
			// validate monthly income is in range
			return res.send({peopleLikeYou: []});
		} else if (typeof req.query.experienced !== 'undefined' && (req.query.experienced != 'false' && req.query.experienced != 'true')) {
			// validate experienced is true or false
			return res.send({peopleLikeYou: []});
		} else if (typeof req.query.latitude !== 'undefined' && (req.query.latitude < config.minLatitude || req.query.latitude > config.maxLatitude)) {
			// validate latitude is in range
			return res.send({peopleLikeYou: []});
		} else if (typeof req.query.longitude !== 'undefined' && (req.query.longitude < config.minLongitude || req.query.longitude > config.maxLongitude)) {
			// validate longitude is in range
			return res.send({peopleLikeYou: []});
		}

		// generate search vector
		var searchVector = investorController.generateVector(req.query.age, req.query.monthlyIncome, req.query.experienced, req.query.age, req.query.monthlyIncome, req.query.experienced);

		// filter criteria by location
		let criteria = {};

		if (typeof req.query.longitude !== 'undefined' && typeof req.query.latitude !== 'undefined') {
			criteria.location = {
	  		"$geoWithin": { // faster because does not sorted results, https://docs.mongodb.com/manual/reference/operator/query/geoWithin/#op._S_geoWithin
	  			"$centerSphere": [
	  				[ req.query.longitude, req.query.latitude ],
	  				config.searchRadius / config.earthRadiusKms // search radius in km, https://docs.mongodb.com/manual/tutorial/calculate-distances-using-spherical-geometry-with-2d-geospatial-indexes/
	  			]
	  		}
	  	};
		}

		// search from database by location first, then add similarity score, sort by score desc, and limit max resultLimits records
	  Investor.find(criteria).then((peopleLikeYou) => {
	  	// formating output
	    res.send({
	    	peopleLikeYou: peopleLikeYou
	    		.map(function (val) {
		    		return {
		    			name: val.name,
		    			age: val.age,
		    			latitude: val.location.coordinates[1],
		    			longitude: val.location.coordinates[0],
		    			monthlyIncome: val.monthlyIncome,
		    			experienced: val.experienced,
		    			score: parseFloat(investorController.computeSimilarity(searchVector, investorController.generateVector(val.age, val.monthlyIncome, val.experienced, req.query.age, req.query.monthlyIncome, req.query.experienced))).toFixed(config.decimalDigits)
		    		};
		    	})
		    	.sort(function (a, b) {
		    		return b.score - a.score;
		    	})
		    	.slice(0, config.resultLimit)
	  	});
	  }, (e) => {
	    res.status(400).send(e);
	  });
	}
};

module.exports = investorController;