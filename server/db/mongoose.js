var mongoose = require('mongoose');
const config = require('../configs/config');

mongoose.Promise = global.Promise;
if (process.env.NODE_ENV !== 'test') {
	mongoose.connect(config.dbProd, config.mongooseCfg);
	mongoose.connection
    .on('error', err => {
      console.warn('Warning', err);
    });
}

module.exports = { mongoose }
