// load library
var express = require('express');
var fileUpload = require('express-fileupload');
var bodyParser = require('body-parser');

// load mongodb connection
var { mongoose } = require('./db/mongoose');

// define app (express)
var app = express();
var port = process.env.PORT || 3000

// install middleware
app.use(bodyParser.json());
app.use(fileUpload());

// load routes
var routes = require('./routes');
routes(app);

// port listener
app.listen(port, () => {
  console.log('Started on port ' + port);
});

// exports app for unit testing purpose
module.exports = { app };

