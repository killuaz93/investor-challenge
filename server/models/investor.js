const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const config = require('../configs/config');

const InvestorSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    minLength: config.nameMinLength
  },
  age: {
    type: Number,
    required: true,
    min: config.minAge,
    max: config.maxAge
  },
  location: {
    type: { type: String, default: 'Point', required: true },
    coordinates: { type: [Number], required: true }
  },
  monthlyIncome: {
    type: Number,
    required: true,
    min: config.minMonthlyIncome,
    max: config.maxMonthlyIncome
  },
  experienced: {
    type: Boolean,
    required: true
  }
});

// InvestorSchema.index({ name: 'text' });

const Investor = mongoose.model('Investor', InvestorSchema);

module.exports = { Investor };