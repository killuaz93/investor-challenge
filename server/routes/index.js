const investorController = require('../controllers/investor_controller'); 

module.exports = (app) => {
	// app.post('/import', investorController.import);
	app.get('/field-min-max', investorController.fieldMinMax);
	app.get('/people-like-you', investorController.peopleLikeYou);
};