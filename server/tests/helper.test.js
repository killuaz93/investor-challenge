const mongoose = require('mongoose');
const config = require('../configs/config');

before(done => {
  mongoose.connect(config.dbTest, config.mongooseCfg);
  mongoose.connection
    .once('open', () => done())
    .on('error', err => {
      console.warn('Warning', err);
      done();
    });
});

// beforeEach(done => {
//   const { investors } = mongoose.connection.collections;
//   investors.drop()
//     .then(() => investors.ensureIndex({ 'geometry.coordinates': '2dsphere' }))
//     .then(() => done())
//     .catch(() => done());
// });