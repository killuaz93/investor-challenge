const expect = require('expect');
const request = require('supertest');

const { app } = require('../../index');
const { investor } = require('../../models/investor');

// positive test
describe('GET /people-like-you?age=23&latitude=40.71667&longitude=19.56667&monthlyIncome=5500&experienced=false', () => {
  it('should get 10 investors', (done) => {
  	request(app)
  		.get('/people-like-you?age=23&latitude=40.71667&longitude=19.56667&monthlyIncome=5500&experienced=false')
  		.expect(200)
  		.expect((res) => {
  			expect(res.body.peopleLikeYou.length).toBe(10);
  		})
  		.end(done)
  });
});

// negative test
describe('GET /people-like-you?age=1000', () => {
  it('should get 0 investors', (done) => {
  	request(app)
  		.get('/people-like-you?age=1000')
  		.expect(200)
  		.expect((res) => {
  			expect(res.body.peopleLikeYou.length).toBe(0);
  		})
  		.end(done)
  });
});

// negative test
describe('GET /people-like-you', () => {
  it('should get 0 investors', (done) => {
  	request(app)
  		.get('/people-like-you')
  		.expect(200)
  		.expect((res) => {
  			expect(res.body.peopleLikeYou.length).toBe(0);
  		})
  		.end(done)
  });
});

// negative test
describe('GET /people-like-you?monthlyIncome=100', () => {
  it('should get 0 investors', (done) => {
  	request(app)
  		.get('/people-like-you?monthlyIncome=100')
  		.expect(200)
  		.expect((res) => {
  			expect(res.body.peopleLikeYou.length).toBe(0);
  		})
  		.end(done)
  });
});

// negative test
describe('GET /people-like-you?latitude=123', () => {
  it('should get 0 investors', (done) => {
  	request(app)
  		.get('/people-like-you?latitude=123')
  		.expect(200)
  		.expect((res) => {
  			expect(res.body.peopleLikeYou.length).toBe(0);
  		})
  		.end(done)
  });
});

// negative test
describe('GET /people-like-you?experienced=yes', () => {
  it('should get 0 investors', (done) => {
  	request(app)
  		.get('/people-like-you?experienced=yes')
  		.expect(200)
  		.expect((res) => {
  			expect(res.body.peopleLikeYou.length).toBe(0);
  		})
  		.end(done)
  });
});